import json

# Function to get the uuid of a setup file, according to the tasks such a 
# function should already exist but I was unable to find it so here it is
def get_id_from_setup(setup_path):
    # Open file at specified path
    f = open(setup_path)
    # Load data in json format
    data = json.load(f)
    # Get dict entry at uuids key
    id = data['JSON']['ID']
    # Return the uuid
    return id 